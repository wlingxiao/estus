package estus.doc
import akka.http.scaladsl.model.{HttpEntity, HttpResponse, ResponseEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteResult.Complete
import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import io.swagger.v3.oas.models.ExternalDocumentation
import akka.http.scaladsl.model.headers.`Content-Type`
import akka.http.scaladsl.model.ContentTypes._
import estus.WebConfig

import scala.io.Source

class SwaggerDocService(config: WebConfig) extends SwaggerHttpService {

  private final val swaggerUiLocation =
    "META-INF/resources/webjars/swagger-ui/3.20.9"

  override val apiClasses: Set[Class[_]] =
    Set(
      classOf[CookieRouterDoc],
      classOf[StatusCodeRouterDoc],
      classOf[HttpMethodRouterDoc]
    )

  override val info = Info(version = "1.0")

  override val externalDocs = Some(
    new ExternalDocumentation()
      .description("Estus Docs")
      .url("/swagger-ui.html")
  )
  override val unwantedDefinitions: Seq[String] =
    Seq("Function1", "Function1RequestContextFutureRouteResult")

  override def routes: Route =
    super.routes ~ pathPrefix("webjars") {
      getFromResourceDirectory(swaggerUiLocation)
    } ~ swaggerUiIndexRoute

  private def swaggerUiIndexRoute = path("swagger-ui.html") {
    get {
      val is = getClass.getClassLoader.getResourceAsStream(
        swaggerUiLocation + "/index.html"
      )
      val indexHtml = Source
        .fromInputStream(is)
        .mkString
        .replaceFirst(
          "https://petstore.swagger.io/v2/swagger.json",
          "/api-docs/swagger.json"
        )
        .replaceAll("""<script src="./""", """<script src="webjars/""")
        .replaceAll(
          """href="./swagger-ui.css"""",
          """href="webjars/swagger-ui.css""""
        )
        .replaceAll("""href="./favicon""", """href="webjars/favicon""")

      complete(HttpEntity(`text/html(UTF-8)`, indexHtml))
    }
  }
}
