import sbt._

object Dependencies {
  private val akkaHttpVersion = "10.1.7"
  private val akkaVersion = "2.5.21"
  private val jacksonModuleScalaVersion = "2.9.8"

  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
  lazy val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % akkaVersion

  lazy val akkaHttp = "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
  lazy val akkaHttpJackson = "com.typesafe.akka" %% "akka-http-jackson" % akkaHttpVersion
  lazy val akkaHttpTestkit = "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion
  lazy val akkaHttpJson = "de.heikoseeberger" %% "akka-http-jackson" % "1.25.2"

  lazy val jacksonModuleScala = "com.fasterxml.jackson.module" %% "jackson-module-scala" % jacksonModuleScalaVersion
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % "1.2.3"

  lazy val swaggerAkkaHttp = "com.github.swagger-akka-http" %% "swagger-akka-http" % "2.0.2"

  lazy val specs2Core = "org.specs2" %% "specs2-core" % "4.5.1"

  lazy val javaxWsRsApi = "javax.ws.rs" % "javax.ws.rs-api" % "2.0.1"
  lazy val swaggerUi = "org.webjars" % "swagger-ui" % "3.20.9"
}
