package estus
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive, Route}
import com.fasterxml.jackson.databind.JsonNode

class HttpMethodRouter extends Router {
  import HttpMethodRouter._
  def context: Directive[Tuple1[RequestInfo]] =
    (extractRequest & parameterMultiMap & extractClientIP & extractUri)
      .tmap(RequestInfo.tupled)

  import Json.marshaller

  def routes: Route =
    getRoute ~ postRoute

  private def getRoute = (path("get") & get & context) { ctx =>
    import Json._
    complete(convertRequestInfoToMap(ctx))
  }

  private def postRoute =
    (path("post") & post & context) { ctx =>
      formFieldMultiMap { forms =>
        val parsedForms = forms.collect {
          case (key, List(value)) =>
            key -> value
          case arg => arg
        }
        complete(convertRequestInfoToMap(ctx) ++ Map("forms" -> parsedForms))
      } ~ entity(as[String]) { str =>
        import Json.unmarshaller
        entity(as[JsonNode]) { jsonNode =>
          complete(
            convertRequestInfoToMap(ctx) ++ Map(
              "json" -> jsonNode,
              "data" -> str
            )
          )
        } ~ complete(convertRequestInfoToMap(ctx) ++ Map("data" -> str))
      }
    }

  private def convertRequestInfoToMap(ctx: RequestInfo): Map[String, Any] = {
    val args = ctx.params.collect {
      case (key, List(value)) =>
        key -> value
      case arg => arg
    }
    val headers = ctx.request.headers.collect {
      case header @ HttpHeader(name, value) if name != "timeout-access" =>
        header.name() -> value
    }.toMap
    val url = ctx.uri.toString()
    Map("args" -> args, "headers" -> headers, "url" -> url)
  }
}

object HttpMethodRouter {
  case class RequestInfo(
      request: HttpRequest,
      params: Map[String, List[String]],
      clientIp: RemoteAddress,
      uri: Uri)

}
