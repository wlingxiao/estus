package estus
import akka.http.scaladsl.server.Route

trait Router {
  def routes: Route
}
