package estus
import akka.http.scaladsl.model.headers.{Cookie, HttpCookie, `Set-Cookie`}
import akka.http.scaladsl.model.{DateTime, HttpRequest}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.RespondWithDirectives.respondWithHeaders
import akka.http.scaladsl.server.{Directive0, Route}

class CookieRouter extends Router {
  import Json.marshaller
  def routes: Route = pathPrefix("cookies") {
    get(getCookieRoute ~ setOrDeleteCookieRoute)
  }

  private def getCookieRoute = (pathEnd & get & extractRequest) { request =>
    complete(Map("cookies" -> getAllCookies(request)))
  }

  private def setOrDeleteCookieRoute =
    (parameterMap & extractRequest) { (params, request) =>
      val cookies = params.map(p => HttpCookie(p._1, p._2, path = Some("/")))
      path("set") {
        setCookies(cookies) {
          complete(Map("cookies" -> (params ++ getAllCookies(request))))
        }
      } ~ path("delete") {
        deleteCookies(cookies) {
          complete(Map("cookies" -> (getAllCookies(request) -- params.keys)))
        }
      }
    }

  private def setCookies(cookies: Iterable[HttpCookie]): Directive0 =
    respondWithHeaders(cookies.toList.map(`Set-Cookie`(_)))

  private def getAllCookies(request: HttpRequest): Map[String, String] =
    request.headers
      .collectFirst {
        case Cookie(c) =>
          c.map(pair => pair.name -> pair.value).toMap
      }
      .getOrElse(Map.empty)

  def deleteCookies(cookies: Iterable[HttpCookie]): Directive0 =
    respondWithHeaders(cookies.toList.map { c ⇒
      `Set-Cookie`(c.copy(value = "deleted", expires = Some(DateTime.MinValue)))
    })
}
