package estus
package doc
import akka.http.scaladsl.server.Route
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import javax.ws.rs.{GET, Path}

@Tag(name = "Cookie", description = "Creates, reads and deletes Cookies")
@Path("/cookies")
private[doc] class CookieRouterDoc {
  @GET
  @Operation(
    summary = "Returns cookie data.",
    responses = Array(new ApiResponse(responseCode = "200", description = "Set cookies."))
  )
  def getCookieRoute: Route = ???

  @GET
  @Path("set")
  @Operation(
    summary = "Sets cookie(s) as provided by the query string and redirects to cookie list.",
    parameters = Array(
      new Parameter(
        name = "name1",
        in = ParameterIn.QUERY,
        description = "cookie name"
      ),
      new Parameter(
        name = "name2",
        in = ParameterIn.QUERY,
        description = "cookie name"
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Redirect to cookie list"
      )
    )
  )
  def setCookieRoute: Route = ???

  @GET
  @Path("delete")
  @Operation(
    summary = "Deletes cookie(s) as provided by the query string and redirects to cookie list.",
    parameters = Array(
      new Parameter(
        name = "name1",
        in = ParameterIn.QUERY,
        description = "cookie name"
      ),
      new Parameter(
        name = "name2",
        in = ParameterIn.QUERY,
        description = "cookie name"
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Redirect to cookie list"
      )
    )
  )
  def deleteCookieRoute: Route = ???

}
