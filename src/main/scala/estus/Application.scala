package estus
import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.RouteResult.Complete
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.LogEntry
import akka.stream.ActorMaterializer
import estus.doc.SwaggerDocService

import scala.concurrent.Await
import scala.concurrent.duration.Duration
object Application {
  implicit val system = ActorSystem("estus-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val webConfig: WebConfig = new WebConfig(system.settings.config)

  def routes: Route = {
    val httpMethodRouter = new HttpMethodRouter
    val statusCodeRouter = new StatusCodeRouter
    val cookieRouter = new CookieRouter
    val swaggerDocService = new SwaggerDocService(webConfig)
    logRequestResult(requestLogger) {
      httpMethodRouter.routes ~
        statusCodeRouter.routes ~
        cookieRouter.routes ~
        swaggerDocService.routes
    }
  }

  def main(args: Array[String]): Unit = {
    val bindingFuture =
      Http().bindAndHandle(routes, webConfig.host, webConfig.port)
    Await.result(bindingFuture, Duration.Inf)
  }

  private def requestLogger: HttpRequest => RouteResult => Option[LogEntry] =
    request => {
      case Complete(response) =>
        Some(LogEntry(formatLog(request, response), level = Logging.InfoLevel))
      case _ => None
    }

  private def formatLog(request: HttpRequest, response: HttpResponse): String = {
    val ua = request.headers
      .find(_.lowercaseName() == "user-agent")
      .getOrElse("")
    val uri = request.uri.toString()
    val method = request.method.value
    val status = response.status
    val version = response.protocol.value
    s""""$ua" "$method $uri $version" $status """
  }
}
