package estus
import com.typesafe.config.Config

class WebConfig(config: Config) {

  def host: String = config.getString("estus.web.host")
  def port: Int = config.getInt("estus.web.port")

}
