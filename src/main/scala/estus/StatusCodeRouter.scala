package estus
import akka.http.scaladsl.model.{HttpResponse, StatusCode, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import scala.util.Random

class StatusCodeRouter extends Router {
  def routes: Route = path("status" / Segment) { codes =>
    (get | post | put | delete | patch) {
      try {
        val codeArray = codes.split(",").map(_.toInt)
        val code = randomElement(codeArray)
        complete(
          HttpResponse(
            status = StatusCodes
              .getForKey(code)
              .getOrElse(customStatusCode(code)))
        )
      } catch {
        case _: NumberFormatException =>
          complete(HttpResponse(status = StatusCodes.BadRequest, entity = "Invalid status code"))
      }
    }
  }

  private def customStatusCode(code: Int): StatusCode =
    StatusCodes
      .custom(code, "", "", isSuccess = true, allowsEntity = true)

  private def randomElement(codes: Array[Int]): Int = {
    assert(codes.nonEmpty)
    codes(Random.nextInt(codes.length))
  }
}
