package estus
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.Specs2RouteTest
import org.specs2.mutable.Specification

class StatusCodeRouterSpec extends Specification with Specs2RouteTest {

  private val statusCodeRouter = new StatusCodeRouter

  private def routes = statusCodeRouter.routes

  "get /status/{codes}" should {
    "return 8848 status code" in {
      Get("/status/8848") ~> routes ~> check {
        status.intValue() should_== 8848
      }
    }

    "return random status code" in {
      Get("/status/200,8848") ~> routes ~> check {
        Seq(200, 8848).contains(status.intValue()) should_== true
      }
    }

    "return 400 status code" in {
      Get("/status/invalid") ~> routes ~> check {
        status should_=== StatusCodes.BadRequest
      }
    }
  }

}
