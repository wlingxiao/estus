package estus.doc
import akka.http.scaladsl.server.Route
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import javax.ws.rs.{GET, POST, Path}
@Tag(name = "HTTP Methods", description = "Testing different HTTP verbs")
class HttpMethodRouterDoc {

  @Path("/get")
  @GET
  @Operation(
    summary = "The request's query parameters.",
    parameters = Array(
      new Parameter(
        name = "name1",
        in = ParameterIn.QUERY,
        description = "parameter name"
      ),
      new Parameter(
        name = "name2",
        in = ParameterIn.QUERY,
        description = "parameter name"
      ),
      new Parameter(
        name = "header1",
        in = ParameterIn.HEADER,
        description = "header name"
      ),
      new Parameter(
        name = "header2",
        in = ParameterIn.HEADER,
        description = "header name"
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "The request’s query parameters."
      )
    )
  )
  def get: Route = ???

  @Path("/post")
  @POST
  @Operation(
    summary = "The request's POST parameters.",
    parameters = Array(
      new Parameter(
        name = "name1",
        in = ParameterIn.QUERY,
        description = "parameter name"
      ),
      new Parameter(
        name = "name2",
        in = ParameterIn.QUERY,
        description = "parameter name"
      ),
      new Parameter(
        name = "header1",
        in = ParameterIn.HEADER,
        description = "header name"
      ),
      new Parameter(
        name = "header2",
        in = ParameterIn.HEADER,
        description = "header name"
      )
    ),
    requestBody = new RequestBody(
      content = Array(
        new Content(
          mediaType = "application/x-www-form-urlencoded",
          schema = new Schema(implementation = classOf[HttpMethodRouterDoc.FormParam])
        ),
        new Content(
          mediaType = "application/json",
          schema = new Schema(implementation = classOf[java.lang.Object])
        )
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "The request’s POST parameters."
      )
    )
  )
  def post: Route = ???

}

private object HttpMethodRouterDoc {

  case class FormParam(
      @Schema(description = "form name") form1: String,
      @Schema(description = "form name") form2: String)

}
