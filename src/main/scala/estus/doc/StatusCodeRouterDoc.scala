package estus.doc
import akka.http.scaladsl.server.Route
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import javax.ws.rs._

@Tag(
  name = "Status code",
  description = "Generates responses with given status code"
)
@Path("/status")
private[doc] class StatusCodeRouterDoc {

  @GET
  @Path("/{codes}")
  @Operation(
    summary = "Return status code or random status code if more than one are given",
    parameters = Array(new Parameter(name = "codes", in = ParameterIn.PATH)),
    responses = Array(
      new ApiResponse(
        responseCode = "100",
        description = "Informational responses"
      ),
      new ApiResponse(responseCode = "200", description = "Success"),
      new ApiResponse(responseCode = "300", description = "Redirection"),
      new ApiResponse(responseCode = "400", description = "Client Errors"),
      new ApiResponse(
        responseCode = "500",
        description = "Internal Server Error"
      )
    )
  )
  def get: Route = ???

  @POST
  @Path("/{codes}")
  @Operation(
    summary = "Return status code or random status code if more than one are given",
    parameters = Array(new Parameter(name = "codes", in = ParameterIn.PATH)),
    responses = Array(
      new ApiResponse(
        responseCode = "100",
        description = "Informational responses"
      ),
      new ApiResponse(responseCode = "200", description = "Success"),
      new ApiResponse(responseCode = "300", description = "Redirection"),
      new ApiResponse(responseCode = "400", description = "Client Errors"),
      new ApiResponse(
        responseCode = "500",
        description = "Internal Server Error"
      )
    )
  )
  def post: Route = ???

  @PUT
  @Path("/{codes}")
  @Operation(
    summary = "Return status code or random status code if more than one are given",
    parameters = Array(new Parameter(name = "codes", in = ParameterIn.PATH)),
    responses = Array(
      new ApiResponse(
        responseCode = "100",
        description = "Informational responses"
      ),
      new ApiResponse(responseCode = "200", description = "Success"),
      new ApiResponse(responseCode = "300", description = "Redirection"),
      new ApiResponse(responseCode = "400", description = "Client Errors"),
      new ApiResponse(
        responseCode = "500",
        description = "Internal Server Error"
      )
    )
  )
  def put: Route = ???

  @DELETE
  @Path("/{codes}")
  @Operation(
    summary = "Return status code or random status code if more than one are given",
    parameters = Array(new Parameter(name = "codes", in = ParameterIn.PATH)),
    responses = Array(
      new ApiResponse(
        responseCode = "100",
        description = "Informational responses"
      ),
      new ApiResponse(responseCode = "200", description = "Success"),
      new ApiResponse(responseCode = "300", description = "Redirection"),
      new ApiResponse(responseCode = "400", description = "Client Errors"),
      new ApiResponse(
        responseCode = "500",
        description = "Internal Server Error"
      )
    )
  )
  def delete: Route = ???

  @HttpMethod("PATCH")
  @Path("/{codes}")
  @Operation(
    summary = "Return status code or random status code if more than one are given",
    parameters = Array(new Parameter(name = "codes", in = ParameterIn.PATH)),
    responses = Array(
      new ApiResponse(
        responseCode = "100",
        description = "Informational responses"
      ),
      new ApiResponse(responseCode = "200", description = "Success"),
      new ApiResponse(responseCode = "300", description = "Redirection"),
      new ApiResponse(responseCode = "400", description = "Client Errors"),
      new ApiResponse(
        responseCode = "500",
        description = "Internal Server Error"
      )
    )
  )
  def patch: Route = ???

}
