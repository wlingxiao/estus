import Dependencies._

organization := "com.github.wlingxiao"
name := "estus"
version := "0.0.1-SNAPSHOT"
scalaVersion := "2.12.8"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-language:implicitConversions",
  "-unchecked"
)

libraryDependencies ++= Seq(
  akkaActor,
  akkaStream,
  akkaSlf4j,
  akkaHttp,
  akkaHttpJackson,
  jacksonModuleScala,
  akkaHttpJson,
  logbackClassic,
  swaggerAkkaHttp,
  javaxWsRsApi,
  swaggerUi,
  akkaHttpTestkit % Test,
  specs2Core % Test
)

assemblyJarName in assembly := "estus.jar"
mainClass in assembly := Some("estus.Application")
